---
title: Nacho Cheese
author: Eduardo Guillen
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

One day a bear was walking down the street when it smelled something 
really cheesy. The smell of cheese was so strong that bear instantly became really hungry.
 The bear found out that the smell was coming from a food truck called "Nachos". 
 The bear walked up to the food truck and asked it what the smell was "It smells good... is that cheese? Can I have some?". 
 The owner responded with " These are Nacho Cheese" The bear was confused... he said " I know they are not mine thats why i'm asking for some nachos"

