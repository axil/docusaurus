---
title: Charlie's New Chocolate Factory
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

As Charlie Bucket grew, his ambitions turned from producing chocolate to that of global empire. Charlie's eventual ambition was to establish a galactic empire, fueled by a fleet of great glass elevators.
