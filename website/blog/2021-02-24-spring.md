---
title: Spring
author: Evan Mathis
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

It's almost Spring time! Snow is melting, the Sun is shining and birds are chirping. Time to get outside and enjoy the beautiful weather. My favorite Spring time activities are running outside, having a picnic and driving with the windows down.

What are your favorite things to do in Spring?
