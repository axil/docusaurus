---
title: Hiking Adventure
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Today I lost someone that I loved. I needed some time to process so I took a hike in the woods. Time went by, the leaves changed, and I came back a new person. I needed some new time as a man of the woods.
